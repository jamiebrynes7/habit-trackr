package com.jamie.android.habittrackr.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;

import com.jamie.android.habittrackr.R;

/**
 * Created by Jamie Brynes on 9/15/2016.
 */
public class CreateHabitDialog extends DialogFragment{

    public interface CreateHabitDialogListener {
        public void onCreateHabitDialogPositive(DialogFragment dialog);
    }

    CreateHabitDialogListener mListener;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        LayoutInflater inflater = getActivity().getLayoutInflater();

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(inflater.inflate(R.layout.dialog_create_habit, null))
                .setMessage(R.string.dialog_create_habit_title)
                .setPositiveButton(R.string.dialog_create_habit_create, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mListener.onCreateHabitDialogPositive(CreateHabitDialog.this);
                    }
                })
                .setNegativeButton(R.string.dialog_create_habit_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        CreateHabitDialog.this.getDialog().cancel();
                    }
                });

        return builder.create();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            mListener = (CreateHabitDialogListener) getTargetFragment();
        }catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + "must implement CreateHabitDialogListener");
        }
    }
}
