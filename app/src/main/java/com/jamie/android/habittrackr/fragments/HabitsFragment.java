package com.jamie.android.habittrackr.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.jamie.android.habittrackr.R;
import com.jamie.android.habittrackr.activities.HabitDetailActivity;
import com.jamie.android.habittrackr.dialogs.CreateHabitDialog;
import com.jamie.android.habittrackr.habit.Habit;
import com.jamie.android.habittrackr.habit.HabitDB;
import com.jamie.android.habittrackr.views.StreakView;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by Jamie Brynes on 8/23/2016.
 */
public class HabitsFragment extends Fragment implements CreateHabitDialog.CreateHabitDialogListener {

    private static final String LOG_TAG = "HabitsFragment.java";

    private HabitDB mHabitsDB;
    private HabitsAdapter mHabitAdapter;


    public HabitsFragment newInstance() {
        HabitsFragment overviewFragment = new HabitsFragment();
        Bundle args = new Bundle();
        overviewFragment.setArguments(args);


        return overviewFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(LOG_TAG, "onCreateView");
        View v = inflater.inflate(R.layout.fragment_habit, container,false);
        mHabitsDB = HabitDB.getInstance(this.getContext());
        mHabitAdapter = new HabitsAdapter(getContext(),android.R.layout.list_content, mHabitsDB.getHabits());
        ListView lv = (ListView) v.findViewById(R.id.habits_list_view);
        lv.setAdapter(mHabitAdapter);

        return v;
    }

    @Override
    public void onResume(){
        super.onResume();
        mHabitAdapter.updateHabitList(mHabitsDB.getHabits());
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        inflater.inflate(R.menu.habits, menu);
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()) {

            case R.id.habits_frag_add_habit:
                DialogFragment dialogFragment = new CreateHabitDialog();
                dialogFragment.setTargetFragment(this, 0);
                dialogFragment.show(getFragmentManager(), "CreateHabitDialog");
                break;

            default:

                break;
        }

        return false;
    }

    @Override
    public void onCreateHabitDialogPositive(DialogFragment dialog) {

        EditText titleEditText = (EditText) dialog.getDialog().findViewById(R.id.habit_create_title);
        String title = titleEditText.getText().toString();

        Habit habit = new Habit(title);
        mHabitsDB.addHabit(habit);

        mHabitAdapter.updateHabitList(mHabitsDB.getHabits());
    }

    private class HabitsAdapter extends ArrayAdapter<Habit> {

        ArrayList<Habit> data;

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            //Retrieve associated habit.
            final Habit habit = getItem(position);

            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.habit_list_item, parent, false);
            }

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = HabitDetailActivity.getIntent(getContext(), habit.getUUID());
                    startActivity(i);
                }
            });

            TextView textView = (TextView) convertView.findViewById(R.id.habit_list_item_title);
            textView.setText(habit.getTitle());


            StreakView streakView = (StreakView) convertView.findViewById(R.id.habit_list_item_streak_view);
            streakView.setFilled(habit.checkCompleted(streakView.getNumCircles()));
            streakView.invalidate();

            ImageButton checkCompleted = (ImageButton) convertView.findViewById(R.id.habit_list_item_btn);
            checkCompleted.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Calendar c = Calendar.getInstance();
                    habit.markTodayCompleted(c.getTime());
                    notifyDataSetChanged();
                }
            });

            //Fill list item view with Habit data.
            return convertView;
        }
        public HabitsAdapter(Context context, int layoutResourceId, ArrayList<Habit> data){
            super(context, layoutResourceId, data);
            this.data = data;
        }

        public void updateHabitList(ArrayList<Habit> habits) {
            data = habits;
            this.notifyDataSetChanged();
        }
    }

}
