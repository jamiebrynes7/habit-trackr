package com.jamie.android.habittrackr.activities;


import android.content.res.Configuration;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.jamie.android.habittrackr.R;
import com.jamie.android.habittrackr.fragments.AnalyticsFragment;
import com.jamie.android.habittrackr.fragments.HabitsFragment;
import com.jamie.android.habittrackr.fragments.OverviewFragment;
import com.jamie.android.habittrackr.fragments.SettingsFragment;
import com.jamie.android.habittrackr.habit.HabitDB;


public class MainActivity extends AppCompatActivity {

    private DrawerLayout mDrawer;
    private Toolbar toolbar;
    private NavigationView mNavDrawer;
    private ActionBarDrawerToggle mDrawerToggle;
    private FragmentManager mFragmentManager;
    private HabitDB mHabitDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mFragmentManager = getSupportFragmentManager();

        //Set a toolbar to replace ActionBar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        //Find our drawer view and set up toggle button
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = setupDrawerToggle();
        mDrawerToggle.syncState();
        mDrawer.addDrawerListener(mDrawerToggle);

        mNavDrawer = (NavigationView) findViewById(R.id.navView);
        setupDrawerContent(mNavDrawer);

        //Get habit database. Initialize for fragments to access a unified database.
        mHabitDB = HabitDB.getInstance(this);

        //Load fragment view
        try {
            Fragment fragment = OverviewFragment.class.newInstance();
            mFragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();
            setTitle("Overview");
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        mHabitDB.writeDatabase();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //The action bar home/up should open or close the drawer
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        switch (item.getItemId()) {
            case R.id.habits_frag_add_habit:
                System.out.println("Pressed");
                break;
        }

        return  super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

     // getMenuInflater().inflate(R.menu.menu, menu);
      return true;
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState){
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig){
        super.onConfigurationChanged(newConfig);

        //Pass any new config to drawer toggle
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    private void setupDrawerContent(NavigationView navigationView)
    {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        selectDrawerItem(menuItem);
                        return true;
                    }
                }
        );
    }

    private ActionBarDrawerToggle setupDrawerToggle() {
        return new ActionBarDrawerToggle(this, mDrawer, toolbar, R.string.drawer_open, R.string.drawer_close);
    }

    private void selectDrawerItem(MenuItem menuItem) {

        Fragment fragment = null;
        if (mFragmentManager.findFragmentByTag(menuItem.getTitle().toString()) != null) {
            fragment = mFragmentManager.findFragmentByTag(menuItem.getTitle().toString());
        }
        else {


            //TODO: Check for existing fragments before instantiating a new one.

            Class fragmentClass;

            //Select correct fragment
            switch (menuItem.getItemId()) {
                case R.id.nav_item_overview:
                    fragmentClass = OverviewFragment.class;
                    break;
                case R.id.nav_item_habits:
                    fragmentClass = HabitsFragment.class;
                    break;
                case R.id.nav_item_analytics:
                    fragmentClass = AnalyticsFragment.class;
                    break;

                case R.id.nav_item_settings:
                    fragmentClass = SettingsFragment.class;
                    break;

                default:
                    fragmentClass = OverviewFragment.class;
            }

            try {
                fragment = (Fragment) fragmentClass.newInstance();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        //Insert fragment
        mFragmentManager.beginTransaction().replace(R.id.flContent, fragment,menuItem.getTitle().toString()).commit();
        //Highlight nav choice
        menuItem.setChecked(true);
        //Set action bar title
        setTitle(menuItem.getTitle());
        //Close the drawer
        mDrawer.closeDrawers();
    }

}
