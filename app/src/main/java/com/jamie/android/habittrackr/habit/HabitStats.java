package com.jamie.android.habittrackr.habit;

import android.util.Log;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by Jamie Brynes on 8/28/2016.
 */
public class HabitStats {

    //Internal variables
    private int mCurrStreak;
    private int mMaxStreak;
    private Date mStartDate;
    private ArrayList<Date> mCompletionRecord;
    private float mPercentage;
    //The daily percentage/total array format: Sun, Mon, Tues, Wed, Thurs, Fri, Sat
    private ArrayList<Float> mDailyPercentage;
    //NOTE: This total = total number of times completed.
    private ArrayList<Integer> mDailyTotal;

    public HabitStats() {

        mDailyPercentage = new ArrayList<Float>();
        //Initialize daily percentage array
        for (int i = 0; i < 7; i++) {
            mDailyPercentage.add(0f);
        }

        mDailyTotal = new ArrayList<Integer>();
        for (int i = 0; i < 7; i++) {
            mDailyTotal.add(0);
        }

        mCompletionRecord = new ArrayList<Date>();
        mStartDate = Calendar.getInstance().getTime();
        mCurrStreak = 0;
        mMaxStreak = 0;
        mPercentage = 0f;

    }

    public HabitStats(int currStreak, int maxStreak, float percentage, ArrayList<Float> dailyPercentage, ArrayList<Integer> dailyTotal, ArrayList<Date> completionRecord, Date startDate) {

        mCurrStreak = currStreak;
        mMaxStreak = maxStreak;
        mPercentage = percentage;
        mDailyPercentage = dailyPercentage;
        mDailyTotal = dailyTotal;
        mCompletionRecord = completionRecord;
        mStartDate = startDate;
    }

    //Update stats when new day is marked.
    public void updateStats() {

        //Find current day.
        DateFormat format = new SimpleDateFormat("yyyy/MM/dd");
        Calendar c = Calendar.getInstance();
        Date curr = c.getTime();

        //Update daily totals and recalculate percentages.
        long total_days = TimeUnit.DAYS.convert(curr.getTime() - mStartDate.getTime(), TimeUnit.MILLISECONDS) + 1;
        int total_completed = mCompletionRecord.size();
        if (total_days == 0) {
            mPercentage = -1;
        }
        else {
            mPercentage = total_completed / total_days;
        }


        //Recalculate streak
        int streak = 0;
        for (int i = mCompletionRecord.size() - 1; i >= 0; i--) {
            if (format.format(mCompletionRecord.get(i)).equals(format.format(curr))) {
                //Increment streak and move curr day back 1
                streak++;
                c.add(Calendar.DATE, -1);
                curr = c.getTime();
            }
            else {
                break;
            }
        }
        mCurrStreak = streak;
        if (mCurrStreak > mMaxStreak) {
            mMaxStreak = mCurrStreak;
        }

        //Now need to update daily percentages. Count number of days between start date and now.
        c = Calendar.getInstance();
        Date date = c.getTime();
        int[] dayCount = new int[7];

        while (date.after(mStartDate)) {

            int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
            dayCount[dayOfWeek - 1] += 1;
            c.add(Calendar.DATE, -1);
            date = c.getTime();
        }

        //Now calculate percentages
        for (int i = 0; i < 7; i++) {
            if (dayCount[i] == 0) {
                mDailyPercentage.set(i, -1f);
            }
            else {
                mDailyPercentage.set(i, (float) mDailyTotal.get(i) / dayCount[i]);
            }
        }

    }

    public void markDayCompleted(Date day)
    {
        if (!checkCompleted(day)) {
            Log.d("HabitStats", "Inserting day");
            //Insert day at the correct point.
            mCompletionRecord.add(day);
            //Increment day of week counter.
            Calendar c = Calendar.getInstance();
            c.setTime(day);
            int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
            mDailyTotal.set(dayOfWeek - 1, mDailyTotal.get(dayOfWeek - 1) + 1);
            updateStats();
        }
    }

    public void removeDayCompleted(Date day) {

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");

        //Search for record and remove it.
        for (int i =0; i < mCompletionRecord.size(); i++) {
            if(dateFormat.format(day).equals(dateFormat.format(mCompletionRecord.get(i)))) {
                mCompletionRecord.remove(i);
                break;
            }
        }

        Calendar c = Calendar.getInstance();
        c.setTime(day);
        int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
        mDailyTotal.set(dayOfWeek - 1, mDailyTotal.get(dayOfWeek - 1) - 1);

    }

    public String serializeData() {

        DateFormat form = new SimpleDateFormat("yyyy/MM/dd/kk/mm/ss");
        String result = "";

        //Serialize the data
        result += mMaxStreak + "," + mCurrStreak + "," + mPercentage + "," + form.format(mStartDate) + ",";
        for (int i = 0; i < mDailyPercentage.size(); i ++) {
            result += mDailyPercentage.get(i).toString() + "," + mDailyTotal.get(i).toString() + ",";
        }
        for (int i = 0; i < mCompletionRecord.size(); i++) {
            result += form.format(mCompletionRecord.get(i)) + ",";
        }
        result = result.substring(0, result.length() - 1);

        return result;
    }

    public boolean checkCompleted(Date d){

        DateFormat df = new SimpleDateFormat("yyyy/MM/dd");


        for (Date day : mCompletionRecord) {
            if (df.format(day).equals(df.format(d))) {
                return true;
            }
        }

        return false;

    }


    //TODO: Add getters for new parameters.
    //Getters
    public int getCurrStreak() {
        return mCurrStreak;
    }

    public int getMaxStreak() {
        return mMaxStreak;
    }

    public float getPercentage() {
        return mPercentage;
    }

    public float getDailyPercentage(int day) {
        return mDailyPercentage.get(day - 1);
    }

    public int getDailyTotal(int day) {
        return mDailyTotal.get(day - 1);
    }

    public ArrayList<Date> getCompletionRecord() {return mCompletionRecord;}
}
