package com.jamie.android.habittrackr.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jamie.android.habittrackr.R;

/**
 * Created by Jamie Brynes on 8/23/2016.
 */
public class OverviewFragment extends Fragment {


    public OverviewFragment newInstance() {
        OverviewFragment overviewFragment = new OverviewFragment();
        Bundle args = new Bundle();
        overviewFragment.setArguments(args);

        return overviewFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_overview, container, false);
    }


}
