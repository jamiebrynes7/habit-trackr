package com.jamie.android.habittrackr.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import com.jamie.android.habittrackr.R;

/**
 * Created by Jamie Brynes on 9/11/2016.
 */
public class StreakView extends View {

    private int mNumCircles = 7;

    private Paint paint_solid_circle;
    private Paint paint_stroke_circle;

    private float circle_radius;
    private float bar_length;
    private float bar_height = 5;
    private float midY;
    private float strokeWidth;

    private boolean[] mFilled = {true, false, true, false, false, true, true};

    public StreakView(Context context, AttributeSet attrs) {
        super(context, attrs);
        parseAttrs(context, attrs);
        setupPaints();
    }

    private void parseAttrs(Context context, AttributeSet attrs) {

        //Load attributes array
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.StreakView,
                0,
                0);

        try {
            mNumCircles = a.getInt(R.styleable.StreakView_numCircles, 7);
        } finally {
            a.recycle();
        }
    }

    private void setupPaints() {
        //Initialize paints
        paint_solid_circle = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint_solid_circle.setColor(getResources().getColor(R.color.colorPrimary));
        paint_solid_circle.setStyle(Paint.Style.FILL_AND_STROKE);
        paint_solid_circle.setStrokeWidth(strokeWidth);

        paint_stroke_circle = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint_stroke_circle.setColor(getResources().getColor(R.color.colorPrimary));
        paint_stroke_circle.setStyle(Paint.Style.STROKE);
        paint_stroke_circle.setStrokeWidth(strokeWidth);
    }
    public void setFilled(boolean[] filled) {
        mFilled = filled;
    }

    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        //Draw the circles
        float cx,cy;

        for (int i = 0; i < mNumCircles; i++) {
            cy = midY;
            //Center of x is number of strokes + number of circles
            cx = (1 + 2*i) * (circle_radius + strokeWidth) + i * bar_length + getPaddingLeft();
            canvas.drawCircle(cx,cy,circle_radius, mFilled[i] ? paint_solid_circle : paint_stroke_circle);
        }


        //Draw the rectangles
        float left,top,right,bottom;
        for (int i = 0; i < mNumCircles - 1; i++) {
            //Calculate dimensions.
            top = midY + bar_height / 2;
            bottom = midY - bar_height / 2;
            left =  2*(i + 1)*(circle_radius + strokeWidth) + i * bar_length + getPaddingLeft();
            right =  2*(i + 1)*(circle_radius + strokeWidth) + (i + 1) * bar_length + getPaddingLeft();
            canvas.drawRect(left,top,right,bottom, paint_solid_circle);
        }
    }

    @Override
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);


        //Set circle's diameter.
        //Select the smallest of the two measurements.
        float xpad = (float)(getPaddingLeft() + getPaddingRight());
        float ypad = (float)(getPaddingTop() + getPaddingBottom());

        float ww = (float) w - xpad;
        float hh = (float) h - ypad;

        midY = hh / 2;

        float y_size = hh;
        //Want bar_length = 3 * radius / 4;
        //Want stroke_width = radius / 3
        //Hence --> total 'radius' required = numCircles * (2 + 2/3) + 0.75(numCircles - 1) = 3.417*numCircles - 0.75
        float x_size = 2*ww / (3.417f* mNumCircles - 0.75f);

        if (y_size > x_size) {
            circle_radius = x_size / 2;
        }
        else {
            circle_radius = y_size / 2;
        }

        bar_length = 3 * circle_radius / 4;
        strokeWidth = circle_radius / 3f;

        paint_solid_circle.setStrokeWidth(strokeWidth);
        paint_stroke_circle.setStrokeWidth(strokeWidth);

    }

    public int getNumCircles() {

        return mNumCircles;
    }

    public void setNumCircles(int i) {
        mNumCircles = i;
        invalidate();
        requestLayout();
    }
}
