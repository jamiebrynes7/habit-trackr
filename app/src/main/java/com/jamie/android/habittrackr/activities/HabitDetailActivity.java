package com.jamie.android.habittrackr.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.jamie.android.habittrackr.R;
import com.jamie.android.habittrackr.habit.Habit;
import com.jamie.android.habittrackr.habit.HabitDB;

import java.util.UUID;

/**
 * Created by Jamie Brynes on 9/11/2016.
 */
public class HabitDetailActivity extends AppCompatActivity {

    private static final String INTENT_EXTRA_TAG = "com.jamie.android.habittrackr.habit_detail_uuid";

    private Habit mHabit;
    private HabitDB db;
    private Toolbar toolbar;

    public static Intent getIntent(Context c, UUID uuid) {
        //Create intent and supply
        Intent i = new Intent(c, HabitDetailActivity.class);
        i.putExtra(INTENT_EXTRA_TAG, uuid.toString());

        return i;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_habit_detail);

        //Get habit by UUID
        db = HabitDB.getInstance(this);
        UUID uuid = UUID.fromString(getIntent().getStringExtra(INTENT_EXTRA_TAG));
        mHabit = db.getHabitByUuid(uuid);

        //Set-up toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        //Set title
        setTitle(mHabit.getTitle());

    }

    @Override
    public void onStop() {
        super.onStop();
        db.writeDatabase();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.habit_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //If hit the back button --> finish activity
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.habit_detail_delete:
                showDeleteAlert();
        }
        return super.onOptionsItemSelected(item);
    }

    private void showDeleteAlert() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle(R.string.delete_habit_title);
        builder.setMessage(getString(R.string.delete_habit_message) + " " + mHabit.getTitle() + "?");

        builder.setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                db.removeHabit(mHabit);
                finish();
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //Do nothing.
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();



    }
}
