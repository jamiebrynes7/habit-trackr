package com.jamie.android.habittrackr.habit;

import android.util.Log;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

/**
 * Created by Jamie Brynes on 8/25/2016.
 */
public class Habit {

    private String mTitle;
    private UUID mUuid;
    private HabitStats mStats;
    private String mGroup;

    private static final String LOG_TAG = "Habit.java";

    //Constructors
    public Habit(String title)
    {
        mTitle = title;
        mUuid = UUID.randomUUID();
        mStats = new HabitStats();
        mGroup = "none";
    }
    public Habit(String title, UUID uuid, HabitStats stats, String group){
        mTitle = title;
        mUuid = uuid;
        mStats = stats;
        mGroup = group;
        mStats.updateStats();
    }

    public void markTodayCompleted(Date day) {
        mStats.markDayCompleted(day);
    }
    public String serializeData() {
        //Format data in CSV format for storage.
        String result = "";
        result += mUuid.toString() + "," + mTitle + "," + mGroup + ",";
        result += mStats.serializeData();
        return result;
    }

    public void updateStats() {
        mStats.updateStats();
    }

    public boolean[] checkCompleted(int num) {

        Calendar c = Calendar.getInstance();
        boolean[] result = new boolean[num];

        for(int i = num - 1; i >= 0; i--) {
            result[i] = mStats.checkCompleted(c.getTime());
            c.add(Calendar.DATE, -1);
        }

        return result;
    }
    //Getters and setters
    public String getTitle(){
        return mTitle;
    }
    public UUID getUUID() { return mUuid;}
    public String getGroup() {
        return mGroup;
    }
    public HabitStats getStats() {
        return mStats;
    }
    public void setGroup(String group) {
        mGroup = group;
    }
}
