package com.jamie.android.habittrackr.habit;


import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.UUID;

/**
 * Created by Jamie Brynes on 8/28/2016.
 */
public class HabitDB {

    private static final String file_name = "habit_data.csv";

    private static HabitDB instance = null;
    private int size;
    private ArrayList<Habit> mHabits;
    private Context mContext;


    private HabitDB(Context context){
        mHabits = new ArrayList<Habit>();
        mContext = context;

        //Check if the file exists and load it if it does.
        File csvFile = new File(context.getFilesDir(), file_name);

        if (csvFile.exists()) {
            loadDatabase();
        }
    }

    public static HabitDB getInstance(Context context) {

        if (instance == null) {
            instance = new HabitDB(context);
        }

        return instance;

    }

    public void addHabit(Habit habit) {
        mHabits.add(habit);
    }

    public void removeHabit(Habit habit) {
        for (int i = 0; i < mHabits.size(); i++) {
            if (mHabits.get(i).getUUID() == habit.getUUID()) {
                mHabits.remove(i);
            }
        }
    }

    public void writeDatabase() {
        //Two parts to this: SQL data base with basic data and CSV file with completion date data.
        Log.d("HabitDB", "Saving database");
        try {
            File csvFile = new File(mContext.getFilesDir(), file_name);
            PrintWriter writer = new PrintWriter(csvFile);
            for (int i = 0; i < mHabits.size(); i++) {
                String line = mHabits.get(i).serializeData();
                writer.println(line);
            }
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void loadDatabase() {

        Log.d("HabitDB", "Loading database");
        String line;
        try {
            DateFormat form = new SimpleDateFormat("yyyy/MM/dd/kk/mm/ss");
            File csvFile = new File(mContext.getFilesDir(), file_name);
            BufferedReader br = new BufferedReader(new FileReader(csvFile));
            //Read the file line by line
            while ((line = br.readLine()) != null) {
                //Split by CSV file
                String[] content = line.split(",");
                //Unpack the contents.
                UUID uuid = UUID.fromString(content[0]);
                String title = content[1];
                String group = content[2];
                int maxStreak = Integer.parseInt(content[3]);
                int currStreak = Integer.parseInt(content[4]);
                float percentage = Float.parseFloat(content[5]);
                Date startDate = form.parse(content[6]);

                ArrayList<Float> dailyPerc = new ArrayList<Float>();
                ArrayList<Integer> dailyTotal = new ArrayList<Integer>();
                ArrayList<Date> completionRecord = new ArrayList<Date>();

                for (int i = 7; i < 21; i+= 2) {
                    dailyPerc.add(Float.parseFloat(content[i]));
                    dailyTotal.add(Integer.parseInt(content[i+1]));
                }

                String[] comp = Arrays.copyOfRange(content, 21, content.length);
                for (int i = 0; i < comp.length; i++) {
                    completionRecord.add(form.parse(comp[i]));
                }
                //Create habit stats and habit object
                HabitStats stats = new HabitStats(currStreak, maxStreak, percentage, dailyPerc, dailyTotal,completionRecord, startDate);
                Habit habit = new Habit(title, uuid, stats, group);

                mHabits.add(habit);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateStats() {

        for (int i = 0; i < mHabits.size(); i++) {
            mHabits.get(i).updateStats();
        }
    }

    public ArrayList<Habit> getHabits() {
        return mHabits;
    }

    public Habit getHabitByUuid(UUID uuid) {
        for (int i = 0; i < mHabits.size(); i++) {
            if (mHabits.get(i).getUUID().equals(uuid)) {
                return mHabits.get(i);
            }
        }

        return null;
    }

}
